<?php
namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Validator\Constraints\Length;

class UploadPictureForm extends AbstractType {
  public function buildForm(FormBuilderInterface $builder, array $options) {
    $builder->add('picture', FileType::class, [
      'required' => TRUE,
      'data' => '',
    ])->add('description', TextType::class, [
      'required' => TRUE,
      'data' => '',
      'attr' => ['placeholder' => 'add description'],
      'constraints' => [
        new Length([
          'min' => 3,
          'max' => 20,
          'minMessage' => 'the login should be minimum 3 characters',
          'maxMessage' => 'the login should be maximum 20 characters',
        ])
      ],
    ])->add('upload file', SubmitType::class);
  }

  public function configureOptions(OptionsResolver $resolver) {
    $resolver->setDefaults(array(
      'data_class' => 'AppBundle\Entity\Picture',
    ));
  }

  public function getName() {
    return 'Save and Add';
  }
}
