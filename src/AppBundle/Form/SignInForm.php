<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Validator\Constraints\Length;

class SignInForm extends AbstractType {
  public function buildForm(FormBuilderInterface $builder, array $options) {
    $builder->add('login', TextType::class, [
      'required' => TRUE,
      'data' => '',
      'attr' => ['placeholder' => 'Login'],
      'constraints' => [
        new Length([
          'min' => 4,
          'max' => 10,
          'minMessage' => 'the login should be minimum 4 characters',
          'maxMessage' => 'the login should be maximum 10 characters',
        ]),
      ],
    ])->add('password', PasswordType::class, [
      'required' => TRUE,
      'data' => '',
      'attr' => ['placeholder' => 'Password'],
      'constraints' => [
        new Length([
          'min' => 5,
          'max' => 10,
          'minMessage' => 'the password should be minimum 5 characters',
          'maxMessage' => 'the password should be maximum 10 characters',
        ]),
      ],
    ])->add('sign me in', SubmitType::class);
  }

  public function configureOptions(OptionsResolver $resolver) {
    $resolver->setDefaults(array(
      'data_class' => 'AppBundle\Entity\User',
    ));
  }

  public function getName() {
    return 'Save and Add';
  }
}
