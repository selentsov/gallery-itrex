<?php
namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Validator\Constraints\Length;

class UpdatePictureInfoForm extends AbstractType {
  public function buildForm(FormBuilderInterface $builder, array $options) {
    $builder->add('description', TextType::class, [
      'required' => TRUE,
      'data' => '',
      'attr' => ['placeholder' => 'update description'],
      'constraints' => [
        new Length([
          'min' => 3,
          'max' => 20,
          'minMessage' => 'the login should be minimum 3 characters',
          'maxMessage' => 'the login should be maximum 20 characters',
        ])
      ],
    ])->add('update description', SubmitType::class);
  }

  public function configureOptions(OptionsResolver $resolver) {
    $resolver->setDefaults(array(
    ));
  }

  public function getName() {
    return 'Save and Add';
  }
}
