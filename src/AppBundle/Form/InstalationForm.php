<?php
namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Validator\Constraints\Length;

class InstalationForm extends AbstractType {
  public function buildForm(FormBuilderInterface $builder, array $options) {
    $builder->add('login', TextType::class, [
      'required' => TRUE,
      'data' => 'admin',
      'attr' => ['placeholder' => 'Login'],
      'constraints' => [
        new Length([
          'min' => 4,
          'max' => 20,
          'minMessage' => 'the login should be minimum 4 characters',
          'maxMessage' => 'the login should be maximum 20 characters',
        ]),
      ],
    ])->add('password', PasswordType::class, [
      'required' => TRUE,
      'data' => 'admin',
      'attr' => ['placeholder' => 'Password'],
      'constraints' => [
        new Length([
          'min' => 5,
          'max' => 20,
          'minMessage' => 'the password should be minimum 5 characters',
          'maxMessage' => 'the password should be maximum 20 characters',
        ]),
      ],
    ])->add('passwordChecking', PasswordType::class, [
      'required' => TRUE,
      'data' => 'admin',
      'attr' => ['placeholder' => 'Repeat password'],
      'constraints' => [
        new Length([
          'min' => 5,
          'max' => 20,
          'minMessage' => 'the password should be minimum 5 characters',
          'maxMessage' => 'the password should be maximum 20 characters',
        ]),
      ],
    ])->add('Create new account', SubmitType::class);
  }

  public function configureOptions(OptionsResolver $resolver) {
    $resolver->setDefaults(array(
    ));
  }

  public function getName() {
    return 'Save and Add';
  }
}
