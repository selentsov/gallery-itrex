<?php
namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ErrorController extends Controller{
  /**
   * @Route("/404", name="notFound")
   */
  public function notFoundAction() {
    return $this->render("error/not-found.html.twig", []);
  }

  /**
   * @Route("/403", name="accessDenied")
   */
  public function accessDeniedAction() {
    return $this->render("error/access-denied.html.twig", []);
  }
}
