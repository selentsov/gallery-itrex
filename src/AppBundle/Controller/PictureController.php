<?php
namespace AppBundle\Controller;

use AppBundle\Entity\Picture;
use AppBundle\Form\UploadPictureForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class PictureController extends Controller {
  /**
   * @Route("/addpicture", name="addNewPicture")
   */
  public function addNewPictureAction(Request $request) {
    if ($request->getSession()->get('role') != 'admin') {
      return $this->redirect($this->generateUrl("accessDenied"));
    }

    $picture = new Picture();
    $form = $this->createForm(UploadPictureForm::class, $picture);
    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
      // $file stores the uploaded image file
      /** @var Symfony\Component\HttpFoundation\File\UploadedFile $file */
      $file = $picture->getPicture();

      // Generate a unique name for the file before saving it
      $fileName = md5(uniqid()) . '.' . $file->guessClientExtension();

      // Move the file to the directory where brochures are stored
      $file->move($this->getParameter('pictures_directory'), $fileName);

      // Update the 'brochure' property to store the file name
      // instead of its contents
      $picture->setPicture($fileName);

      // saving object with info about picture in to database
      $entityManager = $this->getDoctrine()->getEntityManager();
      $entityManager->persist($picture);
      $entityManager->flush();

      return $this->render("upload_picture/upload-picture.html.twig", ['form' => $form->createView()]);
    }

    return $this->render("upload_picture/upload-picture.html.twig", ['form' => $form->createView()]);
  }

  /**
   * @Route("/remove/{id}", name="removePicture")
   */
  public function removePictureAction(Request $request,$id) {
    if ($request->getSession()->get('role') != 'admin') {
      return $this->redirect($this->generateUrl("accessDenied"));
    }

    // get picture from base by id
    $entityManager = $this->getDoctrine()->getEntityManager();
    $picture = $entityManager->getRepository("AppBundle:Picture")->find($id);

    // delete file from storage folder at file system
    unlink($this->getParameter('pictures_directory') . '/' . $picture->getPicture());

    // remove picture from base
    $entityManager->remove($picture);
    $entityManager->flush();

    return $this->redirect($this->generateUrl("gallery"));
  }
}
