<?php
namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Form\RegistrationForm;
use AppBundle\Form\SignInForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;

class UserController extends Controller {
  /**
   * @Route("/registration", name="registration")
   */
  public function registerNewUserAction(Request $request) {
    $form = $this->createForm(RegistrationForm::class);

    // check is form submitted and valid
    $form->handleRequest($request);
    if ($form->isSubmitted() && $form->isValid()) {

      // check if user with inputted login already exists
      // if exists - output error message
      if ($this->getUserFromBase($form->get('login')->getData())) {
        return $this->render("registration/registration.html.twig", [
          'form' => $form->createView(),
          'loginUnavailableError' => TRUE,
        ]);
      }

      // check if passwords match, if not - output error message
      if ($form->get('password')->getData() !== $form->get('passwordChecking')
          ->getData()
      ) {
        return $this->render("registration/registration.html.twig", [
          'form' => $form->createView(),
          'passwordMatchError' => TRUE,
        ]);
      }

      // create new user
      $user = new User();
      $entityManager = $this->getDoctrine()->getEntityManager();

      $user->setLogin($form->get('login')->getData());
      $user->setPassword($form->get('password')->getData());

      // set to all new created users 'user' role (according to the task)
      $user->setRoleId($this->getDoctrine()
        ->getEntityManager()
        ->getRepository('AppBundle:Role')
        ->findOneBy(['role' => 'user']));

      $entityManager->persist($user);
      $entityManager->flush();

      return $this->render("registration/registration.html.twig", [
        'form' => $form->createView(),
        'registred' => TRUE,
      ]);
    }
    return $this->render("registration/registration.html.twig", [
      'form' => $form->createView()]);

  }

  /**
   * @Route("/signin", name="signin")
   */
  public function signInAction() {
    $request = Request::createFromGlobals();
    $form = $this->createForm(SignInForm::class);

    $form->handleRequest($request);
    if ($form->isSubmitted() && $form->isValid()) {

      // check if received data from form hits login / password at the database
      if ($user = $this->getUserFromBase($form->get('login')->getData())) {
        $receivedLogin = $form->get('login')->getData();
        $receivedPassword = $form->get('password')->getData();

        if ($user->getLogin() == $receivedLogin && $user->getPassword() == $receivedPassword) {
          $session = new Session();
          $session->set('role', $this->getUserRoleByLogin($user->getLogin()));
          return $this->redirect($this->generateUrl("gallery"));
        }
      }

      return $this->render("sign_in/sign-In.html.twig", [
        'form' => $form->createView(),
        'error' => TRUE,
      ]);
    }

    return $this->render("sign_in/sign-In.html.twig", [
      'form' => $form->createView(),
      'error' => FALSE,
    ]);
  }

  /**
   * Checks if user with received login already exists in database
   *
   * @param string $login
   *
   * @return \AppBundle\Entity\User|null|object
   */
  private function getUserFromBase($login) {
    return $this->getDoctrine()
      ->getEntityManager()
      ->getRepository('AppBundle:User')
      ->findOneBy(['login' => $login]);
  }

  /**
   * @Route("/logout", name="logout")
   */
  public function logoutAction(Request $request) {
    $request->getSession()->remove('role');
    return $this->redirect($this->generateUrl("signin"));
  }

  /**
   * Gets user's role using his login
   *
   * @param string $login
   *
   * @return \AppBundle\Entity\Role
   */
  private function getUserRoleByLogin($login) {
    $user = $this->getUserFromBase($login);
    $role = $user->getRoleId()->getRole();
    return $role;
  }
}
