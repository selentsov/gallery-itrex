<?php
namespace AppBundle\Controller;

use AppBundle\Entity\Picture;
use AppBundle\Form\UpdatePictureInfoForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class GalleryController extends Controller {
  /**
   * @Route("/gallery/{id}", name="gallery", defaults={"id" = 1})
   */
  public function createGalleryAction($id, Request $request) {
    // if user anonymous - redirect him to '403' page
    if (!$currentUserRole = $request->getSession()->get('role')) {
      return $this->redirect($this->generateUrl("accessDenied"));
    }

    // get all pictures from the base
    $entityManager = $this->getDoctrine()->getEntityManager();
    $pictures = $entityManager->getRepository("AppBundle:Picture")->findAll();

    // add full path to picture's location
    $pictures = $this->preparePicturesForOutput($pictures);

    // pagination
    $pagination = $this->createPagination($pictures, $id);

    return $this->render("gallery/gallery.html.twig", [
      'pictures' => $pictures,
      'pagination' => $pagination,
    ]);
  }

  /**
   * @Route("/picture/{id}", name="picture", defaults={"id" = 1})
   */
  public function pictureAction($id, Request $request) {
    // if user anonymous - redirect him to '403' page
    if (!$currentUserRole = $request->getSession()->get('role')) {
      return $this->redirect($this->generateUrl("accessDenied"));
    }

    // get picture by id
    $entityManager = $this->getDoctrine()->getEntityManager();
    $picture = $entityManager->getRepository("AppBundle:Picture")->find($id);

    $form = $this->createForm(UpdatePictureInfoForm::class);
    $request = Request::createFromGlobals();

    $form->handleRequest($request);
    if ($form->isSubmitted() && $form->isValid()) {
      $picture->setDescription($form->get('description')->getData());
      $entityManager->persist($picture);
      $entityManager->flush();

      return $this->redirect($this->generateUrl("picture", ['id' => $id]));
    }

    $picture = $this->prepareSinglePictureForOutput($picture);

    if ($currentUserRole == 'user') {
      return $this->render("picture/picture.html.twig", ['picture' => $picture]);
    }

    return $this->render("picture/picture-admin.html.twig", [
      'picture' => $picture,
      'form' => $form->createView(),
    ]);
  }

  /**
   * @Route("", name="header")
   */
  public function headerAction(Request $request) {
    return $this->render("header/header.html.twig", [
      'role' => $request->getSession()
        ->get('role'),
    ]);
  }

  /**
   * Sets full path to picture storage according to database and 'config.yml'
   *
   * @param array $pictures
   *
   * @return array
   */
  private function preparePicturesForOutput(array $pictures) {

    foreach ($pictures as $picture) {
      $picture = $this->prepareSinglePictureForOutput($picture);
    }
    $pictures = array_reverse($pictures);

    return $pictures;
  }

  /**
   * Sets full path to picture storage according to database and 'config.yml'
   *
   * @param object Picture
   *
   * @return object Picture
   */
  private function prepareSinglePictureForOutput(&$picture) {
    // get storage directory from
    $pictures_directory = $this->getParameter('pictures_directory');

    // get path to file, cutting of /web/ and all previous directories
    $pictures_directory = substr($pictures_directory, strlen('/web') + strpos($pictures_directory, '/web/'));

    $picture->setPicture($pictures_directory . '/' . $picture->getPicture());

    return $picture;
  }

  /**
   * Prepares pagination parameters and data
   *
   * @param array $pictures
   * @param int $id
   *
   * @return array with parameters for pagination
   */
  private function createPagination(array &$pictures, $id) {
    $pagesAtPaginationOutput = 5;
    $currentPagePossition = 3;
    $picturesOnPage = $this->getParameter('pictures_on_page');

    $pagination['totalPages'] = ceil(count($pictures) / $picturesOnPage);
    $pagination['currentPage'] = $id;
    $pagination['picturesOnPage'] = $picturesOnPage;

    // how many pages will be displayed at the left and right sides
    // from current page
    $pagesBeforeCurrentPage = $pagesAtPaginationOutput - $currentPagePossition;

    // create array with pages numbers for output
    for ($i = $pagination['currentPage'] - $pagesBeforeCurrentPage; $i <= $pagesAtPaginationOutput; $i++) {
      if ($i < 1) {
        continue;
      } elseif ($i > $pagination['totalPages'])
      {
        break;
      }
      $pagination['output'][] = $i;
    }

    $pictures = array_slice($pictures, $id * $picturesOnPage - $picturesOnPage, $picturesOnPage);

    return $pagination;
  }

}
