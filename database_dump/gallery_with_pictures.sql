-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Ноя 29 2016 г., 09:32
-- Версия сервера: 5.7.16-0ubuntu0.16.04.1
-- Версия PHP: 7.0.11-2+deb.sury.org~xenial+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `gallery`
--

-- --------------------------------------------------------

--
-- Структура таблицы `picture`
--

CREATE TABLE `picture` (
  `id` int(11) NOT NULL,
  `picture` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `picture`
--

INSERT INTO `picture` (`id`, `picture`, `description`) VALUES
(1, '156a8f9827f14cca9c4ba5416b2c617e.jpeg', 'paris'),
(2, '673cfdd2bb4169e79ec948ac80627d61.jpeg', 'forest'),
(3, 'eaf76953dc0b372e88be7ca11cc93ceb.jpeg', 'google'),
(4, 'f1174fd948414dfca6097dfc653a0060.jpeg', 'abstract'),
(5, 'be51c3329498539c155d25510582429a.jpeg', 'space'),
(6, 'b96816ac7eefe7928f8016334c089257.jpeg', 'the hero'),
(7, 'b430d7872d898ac25be400b2cd2ce6d4.jpeg', 'the dark lord'),
(8, '6df38e37595274367c30dea9569af181.png', 'space'),
(9, '70e9d17d27d3c330719bc463d1ee4252.jpeg', 'love what you do'),
(10, '7b2f03ad92faf271d5259469ea6a15b4.jpeg', 'love what you do'),
(11, '938c1342ee2bb00bf9f1ca1062857c1b.png', 'earth'),
(12, 'd8a9ba52559efd1c47143853915d937b.jpeg', 'map'),
(13, '3ed858e5b40e3d01e844447c0c0a930e.png', 'eclips'),
(14, '082530b55c023bbfa81709af8e2408df.png', 'elef'),
(15, 'ba5e316bb439900d150335ae2a06f0c4.jpeg', 'buttons'),
(16, 'c61b3fba404cea820a795860bc16d3f8.png', 'phone'),
(17, '3c82c6c2ed7c9d24de93675f45f462ef.jpeg', 'space'),
(18, 'b463cfce8e0b911bfe5a8edcd0caa459.jpeg', 'forest'),
(19, '788bc4bc17dbeb5157a05d1d74b0b4ad.jpeg', 'yoga'),
(20, '832fd7b07dd3cead89b2556015d71226.jpeg', 'night city'),
(21, 'd6d1e77535e2957c497d4842bc6537fe.jpeg', 'space'),
(22, '450be8a01cdde5d8d3edcfb10e53fa1b.jpeg', 'space'),
(23, 'fa71f6645bcf8633bef0b192f66db3eb.jpeg', 'clover'),
(24, '6c6499c9e6544427156c93b689212bac.jpeg', 'brain'),
(25, '0a4c610055dac68850f825e05151fb16.jpeg', 'nature'),
(26, '46787fcb6a1477a30539faf27de6a1ff.jpeg', 'node js');

-- --------------------------------------------------------

--
-- Структура таблицы `role`
--

CREATE TABLE `role` (
  `id` int(11) NOT NULL,
  `role` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `role`
--

INSERT INTO `role` (`id`, `role`) VALUES
(1, 'admin'),
(2, 'user');

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `role_id` int(11) DEFAULT NULL,
  `login` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `role_id`, `login`, `password`) VALUES
(1, 1, 'admin', 'admin');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `picture`
--
ALTER TABLE `picture`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_57698A6A57698A6A` (`role`);

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_8D93D649AA08CB10` (`login`),
  ADD KEY `IDX_8D93D649D60322AC` (`role_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `picture`
--
ALTER TABLE `picture`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT для таблицы `role`
--
ALTER TABLE `role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `FK_8D93D649D60322AC` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
