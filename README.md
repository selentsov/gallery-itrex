gallery
=======

A Symfony project created on November 25, 2016, 8:35 pm.


How to install:

1. clone project from bitbucket   https://bitbucket.org/selentsov/gallery-itrex.git
2. change database settings (use your local settings) at /app/config/config.yml (~ 45 line)
3. use doctrine console command (terminal) $ php bin/console doctrine:database:create
4. go to phpAdmin (or your analog), find database 'gallery' and import sql dump /database_dump/gallery_with.pictures.sql
5. run built in symfony Server - use terminal - $ php bin/console server:run  
6. run project in browser using url received at action #5 (for me its http://127.0.0.1:8000 )

Usage:

1. gallery comes with one defined user 
    login: admin
    password: admin
    
    use to login in to the system
2. you can create new account. Its role will be set to 'user'
